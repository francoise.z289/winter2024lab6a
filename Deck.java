import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor
	public Deck(){
		this.numberOfCards=52;
		this.rng=new Random();
		this.cards=new Card[numberOfCards];
		
		int i=0;
		for(int cardNumber=2;cardNumber<15;cardNumber++){
			//club
			cards[i]=new Card("Club",cardNumber);
			i++;
		}
		for(int cardNumber=2;cardNumber<15;cardNumber++){
			//diamond
			cards[i]=new Card("Diamond",cardNumber);
			i++;
		}
		for(int cardNumber=2;cardNumber<15;cardNumber++){
			//heart
			cards[i]=new Card("Heart",cardNumber);
			i++;
		}
		for(int cardNumber=2;cardNumber<15;cardNumber++){
			//Space
			cards[i]=new Card("Space",cardNumber);
			i++;
		}
			
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public void updateNumberOfCards(){
		this.numberOfCards=this.numberOfCards-1;
	}
	
	public Card drawTopCard(){
		updateNumberOfCards();
		return this.cards[this.length()];
	}
	
	public String toString(){
		String result="";
		for(int i=0; i<this.length(); i++){
			result+=(this.cards[i]+"\t");
		}
		return result;
	}
	
	public void shuffle(){
		//How many time to shuffle
		int numberOfShuffle=this.rng.nextInt(30)+10;
		int positonToShuffle1=0;
		int positonToShuffle2=0;
		Card temp=new Card("null",1);//it's for swap
		for(int i=0;i<numberOfShuffle;i++){
			positonToShuffle1=this.rng.nextInt(this.length());
			positonToShuffle2=this.rng.nextInt(this.length());
			
			//swap
			temp=this.cards[positonToShuffle2];
			this.cards[positonToShuffle2]=this.cards[positonToShuffle1];
			this.cards[positonToShuffle1]=temp;
			
		}
	}
	
}