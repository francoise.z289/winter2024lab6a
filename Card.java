public class Card{
	private String suit;
	private int value;
	
	//constructor
	public Card(String suit,int value){
		this.suit=suit;
		this.value=value;
	}
	
	//getter
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.getValue()+" of "+this.getSuit();
	}
}