
public class LuckyCardGameApp{
	public static void main(String[]args){
		GameManager manager=new GameManager();
		int round=1;
		int totalPoints=0;
		
		System.out.println("Welcome to Lucky Card Game!");
		
		while(manager.getDrawPile().length()>=1&&totalPoints<5){
			System.out.println("===========================================");
			System.out.println("Round "+round+":\n"+manager);
			totalPoints+=manager.calculatePoints();
			System.out.println("Score: "+totalPoints+"\n===========================================");
			round++;
			manager.dealCard();
		}
		
		//decision
		if(totalPoints>=5){
			System.out.println("Congraduation! You win the game with "+totalPoints+"!");
		}else{
			System.out.println("Sorry, you lost the game... your final point is: "+totalPoints);
		}

	}
}