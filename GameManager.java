public class GameManager{
	//field
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//constructor
	public GameManager(){
		this.drawPile=new Deck();
		this.drawPile.shuffle();
		this.centerCard=this.drawPile.drawTopCard();
		this.playerCard=this.drawPile.drawTopCard();
	}
	
	//toString
	public String toString(){
		String result="-----------------------------------------------\nCenter Card: "+this.centerCard+"\nPlayer Card: "+this.playerCard+"\n-----------------------------------------------";
		return result;
	}
	
	//getter
	public Deck getDrawPile(){
		return this.drawPile;
	}
	
	//custom method
	public void dealCard(){
		drawPile.shuffle();
		this.centerCard=drawPile.drawTopCard();
		drawPile.shuffle();
		this.playerCard=drawPile.drawTopCard();
		
	}
	
	public int getNumberOfCard(){
		return this.getDrawPile().length();
	}
	
	public int calculatePoints(){
		if(this.playerCard.getValue()==this.centerCard.getValue()){
			//only the suit is same
			if(this.playerCard.getSuit().equals(this.centerCard.getSuit())){
				//suit and value are same
				return 4;
			}
			return 2;
		}
		return -1;
	}
}